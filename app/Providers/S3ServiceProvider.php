<?php

namespace App\Providers;

use App\Helpers\S3;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class S3ServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('s3', function() {
            return new S3();
        });
    }
}
