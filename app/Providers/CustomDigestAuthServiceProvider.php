<?php

namespace App\Providers;

use App\Helpers\CustomDigestAuth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class CustomDigestAuthServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('customDigestAuth', function() {
            return new CustomDigestAuth();
        });
    }
}
