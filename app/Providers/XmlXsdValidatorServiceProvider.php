<?php

namespace App\Providers;

use App\Helpers\XmlXsdValidator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class XmlXsdValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('xmlXsdValidator', function() {
            return new XmlXsdValidator();
        });
    }
}
