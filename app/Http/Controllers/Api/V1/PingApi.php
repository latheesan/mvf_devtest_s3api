<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\BaseApi;

class PingApi extends BaseApi
{
    /**
     * Simply api availability tester method.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    function ping()
    {
        // Success
        return $this->successResponse('pong');
    }
}
