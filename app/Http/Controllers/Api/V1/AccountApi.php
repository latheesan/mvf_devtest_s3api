<?php

namespace App\Http\Controllers\Api\V1;

use UnexpectedValueException;
use App\Http\Controllers\Api\BaseApi;
use Illuminate\Support\Facades\Cache;

class AccountApi extends BaseApi
{
    /**
     * AccountApi constructor.
     */
    public function __construct()
    {
        // List of field that can be loaded for specified account details
        $this->validAccountFields = ['firstname', 'lastname', 'email', 'telephone', 'balance'];
    }

    /**
     * Method to retrieve details for specified account guid.
     *
     * @param $accountGuid
     * @return \Illuminate\Http\JsonResponse
     */
    function loadDetails($accountGuid)
    {
        // Anticipate errors
        try
        {
            // Load requested account details
            return $this->successResponse($this->loadAccount($accountGuid));
        }
        catch (UnexpectedValueException $unexpectedValueException)
        {
            // Invalid account guid
            return $this->errorResponse($unexpectedValueException->getMessage(), BaseApi::NotFound);
        }
    }

    /**
     * Method to retrieve single field value from account details for specified account guid.
     *
     * @param $accountGuid
     * @param $field
     * @return \Illuminate\Http\JsonResponse
     */
    function loadFieldValue($accountGuid, $field)
    {
        // Check if requested field is valid
        if (!in_array($field, $this->validAccountFields)) {
            return $this->errorResponse('Field '. $field . ' is not valid.', BaseApi::BadRequest);
        }

        // Anticipate errors
        try
        {
            // Load requested account details
            $accountDetails = $this->loadAccount($accountGuid);

            // Load requested field from account details
            return $this->successResponse([
                $field => $accountDetails->$field
            ]);
        }
        catch (UnexpectedValueException $unexpectedValueException)
        {
            // Invalid account guid
            return $this->errorResponse($unexpectedValueException->getMessage(), BaseApi::NotFound);
        }
    }

    /**
     * Helper method to load specified account details.
     *
     * @param $accountGuid
     * @return mixed
     * @throws Exception
     */
    private function loadAccount($accountGuid)
    {
        // Generate cache key
        $accountCacheKey = sprintf('Account_%s_Data', $accountGuid);

        // Check if account data is available in our cache store
        if (!Cache::has($accountCacheKey)) {
            throw new UnexpectedValueException('Account ' . $accountGuid  .' - Not Found');
        }

        // Load requested account details
        return Cache::get($accountCacheKey);
    }
}
