<?php

namespace App\Http\Controllers\Api\V1;

use App\Facades\S3;
use App\Http\Controllers\Api\BaseApi;

class CustomerApi extends BaseApi
{
    /**
     * Method to list all accounts for specified customer guid.
     *
     * @param $customerGuid
     * @return \Illuminate\Http\JsonResponse
     */
    function listAccounts($customerGuid)
    {
        // Load customer files from s3 bucket
        $customerFiles = S3::listCustomerFiles();

        // Check if customer guid exists
        if (!array_key_exists($customerGuid . '.json', $customerFiles)) {
            return $this->errorResponse('Invalid customer guid ' . $customerGuid . ' - Not Found', BaseApi::NotFound);
        }

        // Load requested customer file data
        return $this->successResponse(S3::loadCustomerAccounts($customerGuid));
    }
}
