<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Support\MessageBag;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class BaseApi extends Controller
{
    /**
     * Class constants.
     */
    const Okay = 200;
    const Created = 201;
    const BadRequest = 400;
    const Forbidden = 403;
    const NotFound = 404;
    const InternalServerError = 500;

    /**
     * Method to send success response.
     *
     * @param null $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    function successResponse($data = null, $code = self::Okay)
    {
        return response()->json([
            'data' => $data
        ], $code);
    }

    /**
     * Method to send error response.
     *
     * @param null $message
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    function errorResponse($message = null, $code = self::BadRequest)
    {
        if ($message instanceof MessageBag) {
            $message = $this->getValidatedErrorMessage($message);
        }

        return response()->json([
            'error' => $message
        ], $code);
    }

    /**
     * Method to handle exception and send predictable response.
     *
     * @param $exception
     * @return \Illuminate\Http\JsonResponse
     */
    function handleException($exception)
    {
        // Handle http exception
        if ($exception instanceof HttpException)
        {
            $exception = FlattenException::create($exception);
            $statusCode = (int)$exception->getStatusCode();

            switch ($statusCode) {
                case 400: $errorMessage = 'Bad Request'; break;
                case 403: $errorMessage = 'Forbidden'; break;
                case 404: $errorMessage = 'Not Found'; break;
                case 500: $errorMessage = 'Internal Server Error'; break;
                default: $errorMessage = "Unknown http exception ($statusCode) occurred."; break;
            }

            return $this->errorResponse($errorMessage, $statusCode);
        }

        // Handle normal exception
        $errorMessage = $exception->getMessage();
        if (empty($errorMessage)) {
            $errorMessage = 'Unknown exception occurred.';
        }

        if (App::environment('local')) {
            $errorMessage .= ' On file ' . basename($exception->getFile()) .' at line #'. $exception->getLine();
        }

        return $this->errorResponse($errorMessage, 500);
    }

    /**
     * Extracts all the errors into a single message
     *
     * @param  MessageBag $errors
     * @return string
     */
    function getValidatedErrorMessage(MessageBag $errors)
    {
        $output = '';

        foreach ($errors->all() as $error) {
            $output .= $error . "\n";
        }

        return $output;
    }
}
