<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Facades\CustomDigestAuth;
use App\Http\Controllers\Api\BaseApi;

class CustomAuth
{
    /**
     * Validate custom digest auth.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Anticipate error
        try
        {
            // Validate authentication for this request
            CustomDigestAuth::validate($request);

            // Proceed with the request, user is authenticated
            return $next($request);
        }
        catch (Exception $exception)
        {
            // Authentication error
            return (new BaseApi)->errorResponse($exception->getMessage(), BaseApi::Forbidden);
        }
    }
}
