<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class S3 extends Facade
{
    /**
     * Return the custom facade accessor name.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 's3';
    }
}
