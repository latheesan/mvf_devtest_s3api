<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class XmlXsdValidator extends Facade
{
    /**
     * Return the custom facade accessor name.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'xmlXsdValidator';
    }
}
