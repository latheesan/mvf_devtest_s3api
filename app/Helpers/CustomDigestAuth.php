<?php

namespace App\Helpers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use InvalidArgumentException;
use UnexpectedValueException;

class CustomDigestAuth
{
    /**
     * @var string
     */
    private $authHashAlgo;

    /**
     * @var string
     */
    private $authSecret;

    /**
     * @var string
     */
    private $authHeaderKey;

    /**
     * CustomDigestAuth constructor.
     */
    public function __construct()
    {
        // Set api auth hashing algorithm to use
        $this->authHashAlgo = config('mvf.apiAuthHashAlgo');

        // Set api auth secret
        $this->authSecret = config('mvf.apiAuthSecret');

        // Set api auth header key
        $this->authHeaderKey = config('mvf.apiAuthHeaderKey');
    }

    /**
     * Method to validate custom digest auth from request.
     *
     * @param \Illuminate\Http\Request $request
     * @return bool
     * @throws Exception
     */
    public function validate(Request $request)
    {
        // Check if custom auth header key is present
        if (!$request->hasHeader($this->authHeaderKey)) {
            throw new InvalidArgumentException('Custom digest auth is missing in request header.');
        }

        // Validate & parse auth data from header
        $authData = $request->header($this->authHeaderKey);
        if (!preg_match('/custom-digest/', $authData)) {
            throw new UnexpectedValueException('Authentication header is missing custom-digest value.');
        }
        $authData = str_replace('custom-digest ', '', $authData);
        if (strpos($authData, ':') === false) {
            throw new UnexpectedValueException('Custom digest value appears to be malformed.');
        }
        list ($reqTimestamp, $authDigest) = explode(':', $authData);

        // Check if request timestamp is valid
        if ($reqTimestamp < Carbon::now()->subMinutes(config('mvf.apiAuthTimestampValidForMins'))->timestamp ||
            $reqTimestamp > Carbon::now()->timestamp) {
            throw new Exception('Request header appears to be forged.');
        }

        // Validate auth digest
        if (trim($authDigest) !== $this->createDigest($request)) {
            throw new Exception('Invalid api client credentials.');
        }

        // Authentication was successful
        return true;
    }

    /**
     * Helper method to create custom digest.
     *
     * @param \Illuminate\Http\Request $request
     * @return string
     */
    private function createDigest(Request $request)
    {
        // Init
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = strtoupper($request->getMethod());
        $uriPath = '/' . $request->path();

        // Finished
        return hash(
            $this->authHashAlgo,
            $timestamp . $httpVerb . $uriPath . $this->authSecret
        );
    }
}
