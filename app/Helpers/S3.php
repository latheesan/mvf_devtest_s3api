<?php

namespace App\Helpers;

use UnexpectedValueException;
use InvalidArgumentException;
use App\Facades\XmlXsdValidator;
use Illuminate\Support\Facades\Cache;
use Nathanmac\Utilities\Parser\Facades\Parser;

class S3
{
    /**
     * Public s3 bucket url.
     *
     * @var
     */
    private $s3BucketUrl;

    /**
     * S3 constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        // Load s3 bucket url from env config
        $this->s3BucketUrl = config('mvf.s3BucketUrl');

        // Check if the s3 bucket url is loaded
        if (empty($this->s3BucketUrl)) {
            throw new InvalidArgumentException('Invalid aws s3 bucket url - check .env file to see if AWS_S3_BUCKET is defined.');
        }
    }

    /**
     * Method to list customer files s3 bucket.
     *
     * @return array
     * @throws Exception
     */
    public function listCustomerFiles()
    {
        // Load from cache or live s3 bucket
        return Cache::remember('Customer_Files', config('mvf.cacheMins'), function()
        {
            // Init
            $customerFiles = [];

            // Load s3 bucket contents xml from aws
            $bucketDataXml = @file_get_contents($this->s3BucketUrl);

            // Check if s3 bucket contents xml was loaded
            if ($bucketDataXml === false) {
                throw new UnexpectedValueException('Failed to load s3 bucket contents xml - check url: ' . $this->s3BucketUrl);
            }

            // Proceed if loaded s bucket contents xml validates against xsd schema
            if (XmlXsdValidator::validate($bucketDataXml, config('mvf.listBucketResultXsdPath')))
            {
                // Parse s3 bucket content xml
                $bucketData = Parser::xml($bucketDataXml);

                // Parse bucket contents
                foreach ($bucketData['Contents'] as $content) {
                    $customerFiles[] = $content['Key'];
                }

                // Flip file list array into a hash map for faster look-up
                $customerFiles = array_flip($customerFiles);
            }

            // Finished
            return $customerFiles;
        });
    }

    /**
     * Method to load customer accounts from customer file in s3 bucket.
     *
     * @param $customerGuid
     * @return mixed
     */
    public function loadCustomerAccounts($customerGuid)
    {
        // Load from cache or live s3 bucket
        return Cache::remember(sprintf('Customer_%s_Accounts', $customerGuid), config('mvf.cacheMins'), function() use ($customerGuid)
        {
            // Init
            $customerAccounts = [];

            // Generate file url
            $fileUrl = $this->s3BucketUrl . $customerGuid . '.json';

            // Download requested file from s3 bucket
            $customerFileData = file_get_contents($fileUrl);

            // Check if the requested file was downloaded from s3 bucket
            if ($customerFileData === false) {
                throw new UnexpectedValueException('Failed to load requested file from s3 bucket - ' . $fileUrl);
            }

            // Decode json file data
            $customerFileData = @json_decode($customerFileData);
            if ($customerFileData === null) {
                throw new UnexpectedValueException('Failed to decode json data.');
            }

            // Parse customer accounts & data
            if (isset($customerFileData->accounts) && is_array($customerFileData->accounts)) {
                foreach ($customerFileData->accounts as $account) {
                    $customerAccounts[] = $account->id;
                    $accountDataCacheKey = sprintf('Account_%s_Data', $account->id);
                    if (!Cache::has($accountDataCacheKey)) {
                        unset($account->id);
                        Cache::set($accountDataCacheKey, $account, config('mvf.cacheMins'));
                    }
                }
            }

            // Finished
            return $customerAccounts;
        });
    }
}
