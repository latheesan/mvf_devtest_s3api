<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class ApiAuthTest extends TestCase
{
    private $testRoute;
    private $authHashAlgo;
    private $authSecret;
    private $authHeaderKey;

    protected function setUp()
    {
        parent::setUp();

        $this->testRoute = '/api/v1/ping';
        $this->authHashAlgo = config('mvf.apiAuthHashAlgo');
        $this->authSecret = config('mvf.apiAuthSecret');
        $this->authHeaderKey = config('mvf.apiAuthHeaderKey');
    }

    public function testShouldCheckIfAuthenticationHeaderIsPresentOnRequest()
    {
        $this->json('GET', $this->testRoute)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Custom digest auth is missing in request header.'
            ]);
    }

    public function testShouldCheckIfValidAuthenticationHeaderValueFormatIsPresentOnRequest()
    {
        $header = [
            $this->authHeaderKey => 'bad-data'
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Authentication header is missing custom-digest value.'
            ]);
    }

    public function testShouldCheckIfValidAuthenticationHeaderValueIsPresentOnRequest()
    {
        $header = [
            $this->authHeaderKey => 'custom-digest bad-data'
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Custom digest value appears to be malformed.'
            ]);
    }

    public function testShouldCheckIfExpiredTimestampIsPresentOnAuthenticationHeaderValue()
    {
        $timestamp = Carbon::now()->subMinutes(config('mvf.apiAuthTimestampValidForMins') + 1)->timestamp;
        $dummyHash = hash($this->authHashAlgo, 'dummy-data');
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $dummyHash)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Request header appears to be forged.'
            ]);
    }

    public function testShouldCheckInvalidFutureTimestampIsPresentOnAuthenticationHeaderValue()
    {
        $timestamp = Carbon::now()->addMinutes(1)->timestamp;
        $dummyHash = hash($this->authHashAlgo, 'dummy-data');
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $dummyHash)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Request header appears to be forged.'
            ]);
    }

    public function testShouldFailAuthenticationWhenHttpVerbDoesNotMatchRequest()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'PUT';
        $uriPath = $this->testRoute;
        $authSecret = $this->authSecret;
        $authDigest = hash($this->authHashAlgo, $timestamp . $httpVerb . $uriPath . $authSecret);
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Invalid api client credentials.'
            ]);
    }

    public function testShouldFailAuthenticationWhenUriPathDoesNotMatchRequest()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'GET';
        $uriPath = $this->testRoute . '/bad-uri';
        $authSecret = $this->authSecret;
        $authDigest = hash($this->authHashAlgo, $timestamp . $httpVerb . $uriPath . $authSecret);
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Invalid api client credentials.'
            ]);
    }

    public function testShouldFailAuthenticationWhenAuthSecretDoesNotMatchRequest()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'GET';
        $uriPath = $this->testRoute;
        $authSecret = 'wrong-auth-secret';
        $authDigest = hash($this->authHashAlgo, $timestamp . $httpVerb . $uriPath . $authSecret);
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Invalid api client credentials.'
            ]);
    }

    public function testShouldSucceedForValidAuthenticationInRequestHeader()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'GET';
        $uriPath = $this->testRoute;
        $authSecret = $this->authSecret;
        $authDigest = hash($this->authHashAlgo, $timestamp . $httpVerb . $uriPath . $authSecret);
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(200)
            ->assertExactJson([
                'data' => 'pong'
            ]);
    }

    public function testShouldFailWhenWrongHashingAlgorithmIsUsedToGenerateAuthenticationDigest()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'GET';
        $uriPath = $this->testRoute;
        $authSecret = $this->authSecret;
        $authDigest = hash('md5', $timestamp . $httpVerb . $uriPath . $authSecret);
        $header = [
            $this->authHeaderKey => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];

        $this->json('GET', $this->testRoute, [], $header)
            ->assertStatus(403)
            ->assertExactJson([
                'error' => 'Invalid api client credentials.'
            ]);
    }
}
