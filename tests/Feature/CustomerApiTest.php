<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class CustomerApiTest extends TestCase
{
    private $testRoute;

    protected function setUp()
    {
        parent::setUp();

        $this->testRoute = '/api/v1/customer/a4a06bb0-3fbe-40bd-9db2-f68354ba742f/accounts';
    }

    public function testShouldFailWhenGuidRouteParamDoesNotMatchGloballyDefinedRegExPattern()
    {
        $wrongCustomerGuid = strtoupper('a4a06bb0-3fbe-40bd-9db2-f68354ba742f');
        $this->testRoute = sprintf('/api/v1/customer/%s/accounts', $wrongCustomerGuid);

        $this->json('GET', $this->testRoute, [], $this->generateAuthHeader())
            ->assertStatus(404)
            ->assertExactJson([
                'error' => 'Not Found'
            ]);
    }

    public function testShouldSucceedForValidRequest()
    {
        $this->json('GET', $this->testRoute, [], $this->generateAuthHeader())
            ->assertStatus(200)
            ->assertExactJson([
                'data' => ["02fb54d8-12f4-452e-886c-24fe0219c1b6","0391a1cc-2c00-47b8-9880-aa9fe96bef51","04376b81-9d34-4d25-9e09-f1e6ceec474e","062d13f6-c2c8-4839-beab-f2044fc6c5e3","065136cf-7d0a-4496-8896-7979234c58fb","06d1dc7a-2f0d-4c7d-a90e-c4c6fa27edce","0a712644-9fad-4934-85b9-41745e5b4e27","0dafb276-1620-42ce-bbc5-477209733d5c","12d81c42-506a-4796-891b-ba96e0c5ee67","134b9029-4ace-41ce-9fb9-d203b796179a","17442e15-01e1-4dd6-867e-45521a6be455","17923da6-19cb-4092-8582-00fd16a72f9b","17ee80b8-613d-4e61-9de6-6e52eb1fbd16","1aba397f-c385-4dd2-9c7d-63dd64aa5ff4","1c436428-73bb-46c4-adc9-cd5f2c7d34b4","1d10dd00-1015-468f-a6fb-bc264531be54","200ab890-53e8-4cd5-a8b5-6dfd8b1ec6df","2174c757-180b-4051-846e-4af8eb7a9fb5","21dc2de5-309c-41e9-aba0-0375771eb046","223870c1-c005-47e8-8208-be8aa91de013","245dbde9-cd2e-4794-99e5-92d97ae45a7a","262bdfe3-97fe-49a3-82ed-c8c98305c170","310323bf-f3db-475f-94fc-9f800ae4acc3","32a776f1-4b9e-4c55-8d44-6aa27a4137ee","348709b7-dbb5-4e9e-bcf7-3998f46a5347","3be67d35-39f9-459e-bd3d-3e44c40e1535","3ed29a07-3900-4ebf-af55-395943646400","41f97f86-ff9b-49de-8476-035ed2c4121c","4b90ea55-425d-4bc9-92fe-b74a7b3125b0","4bc611a2-3d54-4e66-a9ac-41d4ee9a2681","50456415-4ea0-42b2-adae-063edce3225c","524ede1e-6413-4e15-b3b9-c0fdf9bdca2a","55f15c00-639f-4585-9430-a8d6a2ae86f7","59c6b0d8-28fb-41aa-8173-6612d46075eb","5ac31fa9-010b-46cd-9903-782dd29c2248","5b39228b-f319-4d5d-bd89-23d54c46625c","5c45b88e-99a9-423e-a781-06bb4843c670","5cc050a1-702e-4cbd-9b56-4da534fe84b8","5ce5b4fa-5e1e-4eaf-b7e8-f32b697883ec","5da19dad-7397-44d4-9f12-73486cb81399","5e06cc25-1f02-4cf6-908e-73a3f5278089"                    ,"5eb7ecb4-4e05-4a39-a41f-a26f7936c6e5","5f341370-525d-4692-9a27-4c09692e259e","62692686-0e56-4a90-8dfe-85b021c9ab14","639bf3d6-17b1-445f-9b33-6108ad64703a","6876e992-994f-4990-9286-9c0edf844697","698a68f1-e157-4a15-b99a-0e590e7eec20","6a1201cf-b9f0-4cf1-83e3-411077a563ab","6a564479-0df8-4c0c-9029-b445289d0db2","6ad4a7d2-307d-413a-82c5-115c8321eede","6d040d13-4086-4167-bb51-742f775e0269","6dcc76fa-1ce8-4e23-8688-5777be17db0e","6ff3f22f-a69a-4798-aea4-39db4f525a8e","7113fe09-0d63-41ce-a329-a7b23094e4ef","73d5789f-2524-4389-99d0-953872c071a7","777e58b3-63bd-4fb7-af9a-bbf7069f1782","81908825-1060-4f64-8f07-5e40a636d34f","861fc585-3313-4928-891d-c8711dfe3f8a","893876a3-46ec-46ff-ab5f-e0345cde5dde","89659ef4-d804-4c8c-b092-e695a3934b1c","8a28f09a-c234-4a95-b1e0-cdbc68979d0a","90e22faf-6f31-4915-8aab-b546e2a2a68e","91c8b4a0-0709-4329-87c5-dad0d480deae","92495f1a-4e0a-4130-98d0-40c9618b240b","96992db8-c9bf-4183-8738-c00c48165859","979d7369-ee72-4e65-89fb-f6bab9709a3c","99a1a8a3-c8ed-448d-a82e-53333dfd151f","9c4ce9dc-92ed-49d0-ad3f-ce634d74483e","9d931267-46e7-46c7-b551-5ad0d63e4edb","a4e55874-8d68-4695-be9f-bdbddc5e0abf","aa7da2d9-96bc-4dfc-a1b6-b3feff4d83ed","abe5bce3-9b31-4ece-acfc-dd8990067c78","abf58203-cc0c-49ca-9d46-bdf8536d78b3","af194f38-7e65-4443-be6c-d265c61a1bc6","b191ead6-e983-4a16-a9a6-07f644275d1f","b1f12e47-ba23-4fac-8a65-0eaa4f9fbd52","b2f81787-6684-460a-a7f8-0b027fe09a71","b5772b44-5b01-46fb-963b-fd4abe75cf5d","b73546b1-aa60-4469-b815-aacb395dbc3e","ba63c1cd-c29f-451e-a73d-1f554e9b63c2","be0438bf-8b0d-4c57-913d-fcafb0bb41f0","be35cf33-6363-4418-9195-412073550867","be9b2a8b-e846-4365-8d5f-0fca4ef9aefb","c35832f6-a634-41f1-9dd4-942116dd5297","c91a46ac-6254-4b17-a98c-1ea17076066d","cba5fac1-edcf-443f-9e99-684a044eec8e","d196241b-2380-4de8-bba9-a5fd56a0c875","d4300618-bac7-458d-b810-79e03bd9e927","da79008f-7176-4fd2-85a1-4272fb79f157","e0d708b9-64b9-49e1-9405-1dbf09dafa2d","e10dee27-8507-4afb-8ce2-106ca05552eb","e9209135-afca-45a9-ae37-88fdc0fe1b90","e99490e2-a1f6-4950-914e-4a9676ee312f","ebd186dc-16ad-4a83-bda8-fca8b4aa2e34","ef9ae01f-2513-4315-aa92-31e432a320dd","f02ff250-5dc3-4e61-b747-dec0d33c0648","f9b7aee5-b0f8-4e1b-a6cf-9b377cff728a","fc5d79dc-d1da-4553-a139-1d6fa828c7d4","fcecddff-a895-4612-bf87-2961d6ba8934","feb30604-8b74-4316-b795-95b99e326f9c"]
            ]);
    }

    public function testShouldFailForInvalidCustomerGuid()
    {
        $wrongCustomerGuid = 'a4a06bb0-3fbe-40bd-9db2-f68354ba742g';
        $this->testRoute = sprintf('/api/v1/customer/%s/accounts', $wrongCustomerGuid);

        $this->json('GET', $this->testRoute, [], $this->generateAuthHeader())
            ->assertStatus(404)
            ->assertExactJson([
                'error' => sprintf('Invalid customer guid %s - Not Found', $wrongCustomerGuid)
            ]);
    }

    /**
     * Helper method to generate valid auth header for current test route.
     *
     * @return array
     */
    private function generateAuthHeader()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'GET';
        $uriPath = $this->testRoute;
        $authSecret = config('mvf.apiAuthSecret');
        $authDigest = hash(config('mvf.apiAuthHashAlgo'), $timestamp . $httpVerb . $uriPath . $authSecret);
        return [
            config('mvf.apiAuthHeaderKey') => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];
    }
}
