<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;

class AccountApiTest extends TestCase
{
    private $testRoute;

    protected function setUp()
    {
        parent::setUp();

        $this->testRoute = '/api/v1/account/861fc585-3313-4928-891d-c8711dfe3f8a';
    }

    public function testShouldFailWhenGuidRouteParamDoesNotMatchGloballyDefinedRegExPattern()
    {
        $wrongAccountGuid = strtoupper('861fc585-3313-4928-891d-c8711dfe3f8a');
        $this->testRoute = sprintf('/api/v1/account/%s', $wrongAccountGuid);

        $this->json('GET', $this->testRoute, [], $this->generateAuthHeader())
            ->assertStatus(404)
            ->assertExactJson([
                'error' => 'Not Found'
            ]);
    }

    /**
     * Helper method to generate valid auth header for current test route.
     *
     * @return array
     */
    private function generateAuthHeader()
    {
        $timestamp = Carbon::now()->timestamp;
        $httpVerb = 'GET';
        $uriPath = $this->testRoute;
        $authSecret = config('mvf.apiAuthSecret');
        $authDigest = hash(config('mvf.apiAuthHashAlgo'), $timestamp . $httpVerb . $uriPath . $authSecret);
        return [
            config('mvf.apiAuthHeaderKey') => sprintf('custom-digest %d:%s', $timestamp, $authDigest)
        ];
    }
}
