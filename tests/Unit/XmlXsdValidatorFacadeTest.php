<?php

namespace Tests\Unit;

use Tests\TestCase;
use ArgumentCountError;
use InvalidArgumentException;
use UnexpectedValueException;
use App\Facades\XmlXsdValidator;

class XmlXsdValidatorFacadeTest extends TestCase
{
    private $validXml;
    private $invalidXml;
    private $xsdPath;

    protected function setUp()
    {
        parent::setUp();

        $this->validXml = file_get_contents(storage_path('data/test/Valid_ListBucketResult.xml'));
        $this->invalidXml = file_get_contents(storage_path('data/test/Invalid_ListBucketResult.xml'));
        $this->xsdPath = config('mvf.listBucketResultXsdPath');
    }

    public function testShouldCheckIfAllArgumentsArePresent()
    {
        $this->expectException(ArgumentCountError::class);
        XmlXsdValidator::validate();
    }

    public function testShouldCheckIfXmlStringArgumentIsPresent()
    {
        $this->expectException(InvalidArgumentException::class);
        XmlXsdValidator::validate('', null);
    }

    public function testShouldCheckIfXsdSchemaPathArgumentIsPresent()
    {
        $this->expectException(InvalidArgumentException::class);
        XmlXsdValidator::validate(null, '');
    }

    public function testShouldCheckIfInvalidXmlStringIsSupplied()
    {
        $this->expectException(UnexpectedValueException::class);
        XmlXsdValidator::validate('invalid-data', $this->xsdPath);
    }

    public function testShouldFailXmlXsdValidationWhenXmlDataDoesNotMatchXsdSchema()
    {
        $this->expectException(UnexpectedValueException::class);
        XmlXsdValidator::validate($this->invalidXml, $this->xsdPath);
    }

    public function testShouldSucceedXmlXsdValidationWhenXmlDataMatchesXsdSchema()
    {
        $this->assertTrue(XmlXsdValidator::validate($this->validXml, $this->xsdPath));
    }
}
