<?php

/**
 * MVF Rest API Application Configuration
 */
return
[
    's3BucketUrl' => env('AWS_S3_BUCKET', ''),                               // Public s3 bucket web url
    'cacheMins' => env('CACHE_MINS', 1440),                                  // How long to keep data cached, defaults to 1 day (if not specified in env)
    'listBucketResultXsdPath' => storage_path('data/AWS_S3_ListBucketResult.xsd'), // Path to aws s3 list bucket result xsd schema (used for xml validation)
    'apiAuthHashAlgo' => env('API_AUTH_HASH_ALGO', 'sha256'),                // Hashing algorithm to use in the custom digest auth
    'apiAuthSecret' => env('API_AUTH_SECRET', ''),                           // Api secret key (used in the custom digest)
    'apiAuthHeaderKey' => env('API_AUTH_HEADER_KEY', 'Authentication'),      // Where to expect the auth data in the request header
    'apiAuthTimestampValidForMins' => 15,                                                // How long in minutes the request timestamp is considered valid for
];
