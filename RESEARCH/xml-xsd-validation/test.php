<?php

$xml_string = '<?xml version="1.0" encoding="UTF-8"?><ListBucketResult xmlns="http://s3.amazonaws.com/doc/2006-03-01/"><Name>mvf-devtest-s3api</Name><Prefix></Prefix><Marker></Marker><MaxKeys>1000</MaxKeys><IsTruncated>false</IsTruncated><Contents><Key>a4a06bb0-3fbe-40bd-9db2-f68354ba742f.json</Key><LastModified>2017-07-11T16:39:50.000Z</LastModified><ETag>&quot;7b704d91aee1d170c1ab2aba24ffad80&quot;</ETag><Size>17658</Size><StorageClass>STANDARD</StorageClass></Contents><Contents><Key>be0438bf-8b0d-4c57-913d-fcafb0bb41f0.json</Key><LastModified>2017-07-11T16:39:50.000Z</LastModified><ETag>&quot;aced4a1583640f3dc11c716b6cc947e5&quot;</ETag><Size>17724</Size><StorageClass>STANDARD</StorageClass></Contents><Contents><Key>be9b2a8b-e846-4365-8d5f-0fca4ef9aefb.json</Key><LastModified>2017-07-11T16:39:50.000Z</LastModified><ETag>&quot;407e204602859ee4371d6f37e3744b64&quot;</ETag><Size>17606</Size><StorageClass>STANDARD</StorageClass></Contents></ListBucketResult>';

function libxml_display_error($error)
{
    $return = "<br/>\n";
    switch ($error->level) {
        case LIBXML_ERR_WARNING:
            $return .= "<b>Warning $error->code</b>: ";
            break;
        case LIBXML_ERR_ERROR:
            $return .= "<b>Error $error->code</b>: ";
            break;
        case LIBXML_ERR_FATAL:
            $return .= "<b>Fatal Error $error->code</b>: ";
            break;
    }
    $return .= trim($error->message);
    if ($error->file) {
        $return .=    " in <b>$error->file</b>";
    }
    $return .= " on line <b>$error->line</b>\n";

    return $return;
}

function libxml_display_errors() {
    $errors = libxml_get_errors();
    foreach ($errors as $error) {
        print libxml_display_error($error);
    }
    libxml_clear_errors();
}

// Enable user error handling
libxml_use_internal_errors(true);

$xml = new DOMDocument(); 
$xml->loadXML($xml_string); 

if (!$xml->schemaValidate('test.xsd')) {
    print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
    libxml_display_errors();
} else {
    echo 'XML XSD Validation OK';
}


?>