<?php

use App\Facades\S3;
use App\Facades\CustomDigestAuth;
use App\Facades\XmlXsdValidator;
use Carbon\Carbon;

Route::get('/', function () {
    return view('welcome');
});

// TODO - remove test routes
Route::get('test', function()
{
    // return Parser::xml(file_get_contents(env('FAKE_AWSS3_BUCKET_URL')));

    //$expiresAt = \Carbon\Carbon::now()->addMinutes(10);
    //Cache::put('key', 'value', $expiresAt);
    //return Cache::get('key');

    // return file_get_contents('http://mvf-devtest-s3api.s3-eu-west-1.amazonaws.com');
    // return Storage::disk('s3')->get('a4a06bb0-3fbe-40bd-9db2-f68354ba742f.json');

    // return S3::test();

    return CustomDigestAuth::validate(new \Illuminate\Http\Request()) ? 'true' : 'false';
});
Route::get('test/{guid}', function($guid) {
    return $guid;
});
Route::get('test/xml-validation', function() {
    return XmlXsdValidator::validate(file_get_contents(env('AWS_S3_BUCKET')), storage_path('data/AWS_S3_ListBucketResult.xsd')) ? 'true' : 'false';
});
Route::get('test/dummy-api-server', function(\Illuminate\Http\Request $request)
{
    $timestamp = Carbon::now()->timestamp;
    $httpVerb = strtoupper($request->getMethod());
    $uriPath = '/'. $request->path();
    $secret = config('mvf.apiAuthSecret');

    $authData = str_replace('custom-digest ', '', $request->header('Authentication'));
    list ($reqTimestamp, $customDigest) = explode(':', $authData);

    if ($reqTimestamp > Carbon::now()->addMinutes(15)->timestamp) {
        return response()->json('Invalid auth', 403);
    }

    $checkDigest = hash('sha256', $timestamp . $httpVerb . $uriPath . $secret);

    return response()->json([
        'data' => ($customDigest === $checkDigest) ? 'Auth OK' : 'Auth Error'
    ]);
});
Route::get('test/dummy-api-client', function()
{
    $timestamp = Carbon::now()->timestamp;
    $httpVerb = 'GET';
    $uriPath = '/test/dummy-api-server';
    $secret = config('mvf.apiAuthSecret');

    $digest = hash('sha256', $timestamp . $httpVerb . $uriPath . $secret);

    $httpClient = new GuzzleHttp\Client;
    $res = $httpClient->get(url($uriPath), [
        'headers' => [
            'Authentication' => "custom-digest $timestamp:$digest"
        ]
    ]);

    echo $res->getStatusCode(); // 200
    echo '<br>';
    echo $res->getBody();
});
Route::get('test/api-ping', function()
{
    $timestamp = Carbon::now()->timestamp;
    $httpVerb = 'GET';
    $uriPath = '/api/v1/ping';
    $secret = config('mvf.apiAuthSecret');

    $digest = hash('sha256', $timestamp . $httpVerb . $uriPath . $secret);

    $httpClient = new GuzzleHttp\Client;
    $res = $httpClient->get(url($uriPath), [
        'headers' => [
            'Authentication' => "custom-digest $timestamp:$digest"
        ]
    ]);

    echo $res->getStatusCode(); // 200
    echo '<br>';
    echo $res->getBody();
});
// TODO - remove test routes
