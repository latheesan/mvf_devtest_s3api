<?php

/**
 * API V1 Routes
 */
Route::group(['middleware' => ['custom.auth', 'throttle'], 'namespace' => 'Api\V1', 'prefix' => 'v1'], function()
{
    // ping endpoint
    Route::get('ping', 'PingApi@ping');

    // customer endpoints
    Route::group(['prefix' => 'customer'], function() {
       Route::get('{guid}/accounts', 'CustomerApi@listAccounts');
    });

    // account endpoints
    Route::group(['prefix' => 'account'], function() {
        Route::get('{guid}', 'AccountApi@loadDetails');
        Route::get('{guid}/{field}', 'AccountApi@loadFieldValue');
    });
});
